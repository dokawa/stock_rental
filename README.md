# Stock Rental Scrapper
A small script that scrappes from Bovespa (the brazilian stocks exchange) to compare stock rental values.

The values are scattered by letter, so I developed this scrapper to compare and see the best options.

It's a refactoring of [aluguel_ações](https://gitlab.com/dokawa/aluguel_acoes).

