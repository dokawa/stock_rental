from bs4 import BeautifulSoup


class CompanyPage:

    def __init__(self, html):
        self.html = BeautifulSoup(html, features="html.parser")

    def get_mean_rent_value_map(self):
        table_lines = self.html.find('tbody')
        lines = self.parse_table_lines(table_lines)
        value_map = {}
        for line in lines:
            name = line[0]
            weighted_mean_return = line[6]
            value_map[name] = weighted_mean_return
        return value_map

    def parse_table_lines(self, table_lines):
        values = []
        if not table_lines == None:
            for line in table_lines:
                cleaned_line = self.remove_html_tags(line)
                column_values = self.get_column_values(cleaned_line)
                values.append(column_values)
        return values

    def remove_html_tags(self, element):
        # the first line puts a placeholder on company name spaces
        return str(element).replace(' ', '#') \
                            .replace('<tr><td>', '') \
                            .replace('</td></tr>', '') \
                            .replace('<td>', '') \
                            .replace('<td#class="text-right">', '') \
                            .replace('</td>', ' ') \
                            .replace('</tr>', '') \
                            .replace('%', '') \
                            .replace('.', '') \
                            .replace(',', '.')

    def get_column_values(self, line):
        # prevent from separating the name of the company into parts
        columns = line.rsplit(' ')
        return columns









