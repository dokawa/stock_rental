from bs4 import BeautifulSoup


class LetterPage:

    def __init__(self, html):
        self.html = BeautifulSoup(html, features="html.parser")

    def get_companies_list(self):
        companies_list_elements = self.html.find_all('tr')
        self.drop_title(companies_list_elements)
        companies_list = []
        for element in companies_list_elements:
            name = element.get_text()
            companies_list.append(name)
        return companies_list

    # The title is part of the table, we need to drop it
    def drop_title(self, category_list):
        if category_list:
            category_list.pop(0)

