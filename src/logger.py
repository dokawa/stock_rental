from datetime import datetime


def log(message):
    print("[" + str(get_current_timestamp()) + "] " + message)


def get_current_timestamp():
    now = datetime.now()
    timestamp = datetime.timestamp(now)
    return datetime.fromtimestamp(timestamp)
