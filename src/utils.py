import requests
from requests.adapters import HTTPAdapter
from urllib3 import Retry
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)



def make_request(url):
    session = get_requests_session()
    try:
        response = session.get(url, headers=get_headers(), verify=False)
        return response
    except Exception:
        print("Error while making request to page")


def get_requests_session():
    # Return requests session with retries configured
    session = requests.Session()
    retries = Retry(total=3, backoff_factor=0.5)
    session.mount("https", HTTPAdapter(max_retries=retries))
    return session

def get_headers():
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) "
        "Chrome/87.0.4280.141 Safari/537.36",
        "Accept": "application/json, text/plain, */*",
        "Accept-Encoding": "gzip, deflate, br"
    }
    return headers