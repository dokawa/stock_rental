import base64
import concurrent.futures
import json

from src import logger
from src.utils import make_request

PAGE_SIZE = 60

LIST_BASE_URL = "https://sistemaswebb3-listados.b3.com.br/securitiesLendingProxy/SecuritiesCall/GetListedRegistered/"

DETAIL_BASE_URL = "https://sistemaswebb3-listados.b3.com.br/securitiesLendingProxy/SecuritiesCall/GetDetailRegistered/"


def log_values(value_map):
    for code, value in value_map.items():
        logger.log(code + ": " + value)

def get_json_from_response(response_content):
    try:
        json_content = json.loads(response_content)
        return json_content
    except Exception:
        logger.exception("Error while parsing response to json")


def get_main_page_code(page_number):
    message = f'{{"pageNumber":{page_number},"pageSize":{PAGE_SIZE}}}'
    return convert_to_base_64(message)


def get_company_page_code(company_abbreviation):
    message = f'{{"index":"{company_abbreviation}"}}'
    return convert_to_base_64(message)


def convert_to_base_64(string):
    message_bytes = string.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')
    return base64_message


def get_main_page_url(page_number):
    return f"{LIST_BASE_URL}{get_main_page_code(page_number)}"
# get_rental_values('src/stock_rental.json')


def get_company_page_url(company_abbreviation):
    return f"{DETAIL_BASE_URL}{get_company_page_code(company_abbreviation)}"


def get_max_value(detail_json):
    return detail_json.get("donorMax")


def get_min_value(detail_json):
    return detail_json.get("donorMin")


def get_average_value(detail_json):
    return detail_json.get("donorWeightedAverage")


def get_company_name(detail_json):
    return detail_json.get("name")


def get_asset_name(detail_json):
    return detail_json.get("asset")


def get_responses_from_urls(urls):
    with concurrent.futures.ThreadPoolExecutor() as executor:  # optimally defined number of threads
        responses = [executor.submit(make_request, url) for url in urls]
        concurrent.futures.wait(responses)

    results = []
    for response in responses:
        results.append(response.result())

    return results


def sort_dict_by_value(dict):
    return {k: v for k, v in sorted(dict.items(), key=lambda item: item[1], reverse=True)}


page_urls = []
for i in range(20):
    url = get_main_page_url(i)
    page_urls.append(url)

responses = get_responses_from_urls(page_urls)

company_urls = []
for response in responses:
    page_json = get_json_from_response(response.text)
    results = page_json.get("results")
    for result in results:
        abbreviation = result.get("index")
        company_page_url = get_company_page_url(abbreviation)
        company_urls.append(company_page_url)

responses = get_responses_from_urls(company_urls)

values = {}
for response in responses:
    detail_json = get_json_from_response(response.content)
    # print(detail_json)
    if type(detail_json) is list:
        for company_json in detail_json:
            name = get_asset_name(company_json)
            value = get_average_value(company_json)
            values[name] = float(value)
            print(f"{name}: {value}")
    else:
        name = get_asset_name(detail_json)
        value = get_average_value(detail_json)
        values[name] = float(value)
        print(f"{name}: {value}")

dict = sort_dict_by_value(values)
for key, value in dict.items():
    print(f"{key}: {value}")










