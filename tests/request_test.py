import unittest

from bs4 import BeautifulSoup
from pip._vendor import requests

from src.CompanyPage import CompanyPage
from src.LetterPage import LetterPage
from stock_rental import join_map, LIST_URL_PATTERN, get_yesterday_date_string, \
    COMPANY_URL_PATTERN, take


class RequestTest(unittest.TestCase):

    @unittest.skip
    def test_get_companies_list(self):
        url = LIST_URL_PATTERN % ('A', get_yesterday_date_string())
        html = requests.get(url)
        companies_list = LetterPage(html.content).get_companies_list()
        for company in companies_list:
            self.assertTrue(company.startswith('A'))

    @unittest.skip
    def test_company_page(self):
        name = 'AES TIETE ENERG'
        nested_html = requests.get(
            COMPANY_URL_PATTERN % (name, get_yesterday_date_string()))
        parsed_nested_html = BeautifulSoup(nested_html.content)
        table_lines = parsed_nested_html.find('tbody')
        self.assertEqual(len(CompanyPage(nested_html.content).parse_table_lines(table_lines)[0]), 11)

    @unittest.skip
    def test_same_key_join(self):
        first = {"key1": "value1", "key2": "value2"}
        second = {"key2": "value2", "key3": "value"}
        self.assertEqual(len(join_map(first, second).items()), 3)

    def test_take(self):
        FILE_PATH = 'hue'
        print(take(5, '../src/stock_rental.json'))


if __name__ == '__main__':
    unittest.main(warnings='ignore')
